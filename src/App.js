import React, { Component } from "react";
// import {todos as todosList} from "./todos";
import todosList from "./todos.json";
// import { v4 as uuid } from "uuid";
import "./App.css";

class App extends Component {
  // const [todos, setTodos] = useState(todosList);
  // const [inputText, setInputText] = useState("");
  state = {
    todos: todosList,
    inputText: "",
  };

  handleChange = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };

  handleAddTodo = (event) => {
    if (event.which === 13) {
      const newTodos = this.state.todos;
      const newTodo = {
        userId: 1,
        id: Math.random(),
        title: this.state.inputText,
        completed: false,
      };
      newTodos.push(newTodo);
      this.setState({ todos: newTodos, inputText: "" });
    }
  };

  handleDeleteTodo = (todoId) => (e) => {
    const newTodos = this.state.todos.filter((todo) => todo.id !== todoId);
    this.setState({ todos: newTodos });
  };

  handleToggle = (todoId) => (e) => {
    const newTodos = this.state.todos;
    newTodos.forEach((todo) => {
      if (todo.id === todoId) {
        todo.completed = !todo.completed;
      }
    });
    this.setState({ todos: newTodos });
  };

  handleClearCompletedToDos = () => {
    const newTodos = this.state.todos.filter(
      (todo) => todo.completed === false
    );
    this.setState({ todos: newTodos });
  };
  render() {
    return (
      <section className="todoapp">
        <header className="header">
          <h1 style={{ color: "green" }}>TO-DO'S</h1>
          <input
            className="new-todo"
            name="inputText"
            value={this.state.inputText}
            onChange={this.handleChange}
            onKeyDown={this.handleAddTodo}
            placeholder="What needs to be done?"
            autoFocus
          />
        </header>
        <TodoList
          todos={this.state.todos}
          handleToggle={this.handleToggle}
          handleDeleteTodo={this.handleDeleteTodo}
        />
        <footer className="footer">
          <span className="todo-count">
            <strong>
              {this.state.todos.filter((todo) => !todo.completed).length}
            </strong>{" "}
            item(s) left
          </span>
          <button
            className="clear-completed"
            onClick={this.handleClearCompletedToDos}
          >
            Clear Completed
          </button>
        </footer>
      </section>
    );
  }
}

class TodoItem extends Component {
  render() {
    return (
      <li className={this.props.completed ? "completed" : ""}>
        <div className="view">
          <input
            className="toggle"
            type="checkbox"
            checked={this.props.completed}
            onChange={this.props.handleToggle}
          />
          <label>{this.props.title}</label>
          <button className="destroy" onClick={this.props.handleDeleteTodo} />
        </div>
      </li>
    );
  }
}

class TodoList extends Component {
  render() {
    return (
      <section className="main">
        <ul className="todo-list">
          {this.props.todos.map((todo) => (
            <TodoItem
              title={todo.title}
              completed={todo.completed}
              id={todo.id}
              handleToggle={this.props.handleToggle(todo.id)}
              handleDeleteTodo={this.props.handleDeleteTodo(todo.id)}
              key={todo.id}
            />
          ))}
        </ul>
      </section>
    );
  }
}

export default App;
